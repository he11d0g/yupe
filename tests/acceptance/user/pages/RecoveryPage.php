<?php
class RecoveryPage
{
    public static $URL = '/recovery';

    public static $emailField = 'RecoveryForm[email]';

    public static $buttonLabel = 'Восстановить пароль';
}